FROM python:3.8

WORKDIR /app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install psycopg2 and gdal
RUN apt-get update && \
    apt-get install -y postgresql-client gdal-bin && \
    rm -rf /var/lib/apt/lists/*

# copy project
COPY . /app/

# install dependencies
RUN pip install --upgrade pip
RUN pip install -r /app/requirements.txt