from typing import Dict
from django.conf import settings
import requests


class IPStackException(Exception):
    pass


class IPStack:
    def __init__(self):
        if settings.IPSTACK_ACCESS_KEY:
            self.params = {"access_key": settings.IPSTACK_ACCESS_KEY}

    def lookup(self, address: str) -> Dict:
        try:
            request = requests.get(
                f"http://api.ipstack.com/{address}", params=self.params
            )
        except requests.exceptions.RequestException as e:
            raise IPStackException("Cannot connect to IPStack")
        if request.status_code != requests.codes.ok:
            raise IPStackException
        response = request.json()
        if "success" in response and not response["success"]:
            raise IPStackException(response["info"])
        if "latitude" in response and not response["latitude"]:
            raise IPStackException("IPStack returned invalid response")
        return response
