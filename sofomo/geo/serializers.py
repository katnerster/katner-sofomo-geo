from rest_framework import serializers, validators
from sofomo.geo.models import GeolocationData


class GeolocationDataSerializer(serializers.ModelSerializer):
    ip = serializers.CharField(required=False, default=None)
    point = serializers.CharField(read_only=True)

    def validate(self, data):
        if data["ip"] and data["url"]:
            raise serializers.ValidationError("Only ip or url can be provided")
        return data

    def run_validators(self, value):
        for validator in self.validators:
            if isinstance(validator, validators.UniqueTogetherValidator):
                self.validators.remove(validator)
        super().run_validators(value)

    def create(self, validated_data):
        instance, _ = GeolocationData.objects.get_or_create(**validated_data)
        return instance

    class Meta:
        model = GeolocationData
        fields = [
            "id",
            "ip",
            "url",
            "point",
            "continent",
            "country",
            "region",
            "city",
            "zip_code",
        ]
