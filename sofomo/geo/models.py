from django.db import models
from django.contrib.gis.db.models import PointField


class GeolocationData(models.Model):
    ip = models.GenericIPAddressField()
    url = models.TextField(blank=True)
    point = PointField()
    continent = models.TextField(blank=True, null=True)
    country = models.TextField(blank=True, null=True)
    region = models.TextField(blank=True, null=True)
    city = models.TextField(blank=True, null=True)
    zip_code = models.TextField(blank=True, null=True)

    class Meta:
        unique_together = ["ip", "url"]
