from django.contrib.gis.geos import Point
from sofomo.geo.models import GeolocationData
from sofomo.geo.serializers import GeolocationDataSerializer
from sofomo.geo.ipstack import IPStack, IPStackException
from rest_framework import viewsets
from rest_framework.exceptions import APIException
from rest_framework.status import HTTP_400_BAD_REQUEST


class GeolocationDataViewSet(viewsets.ModelViewSet):
    queryset = GeolocationData.objects.all()
    serializer_class = GeolocationDataSerializer

    def perform_create(self, serializer):
        api = IPStack()
        request_data = ""
        if "ip" in self.request.data:
            request_data = self.request.data["ip"]
        elif "url" in self.request.data:
            request_data = self.request.data["url"]
        else:
            raise APIException("No data provided")
        try:
            response = api.lookup(request_data)
        except IPStackException as e:
            raise APIException(e.args[0] if len(e.args) else None)
        data_to_save = {"ip": response["ip"]}
        if self.request.data["url"]:
            data_to_save["url"] = self.request.data["url"]
        if "latitude" in response:
            data_to_save["point"] = Point(response["longitude"], response["latitude"])
        data_to_save["continent"] = response["continent_name"]
        data_to_save["country"] = response["country_name"]
        data_to_save["region"] = response["region_name"]
        data_to_save["city"] = response["city"]
        data_to_save["zip_code"] = response["zip"]
        serializer.save(**data_to_save)
