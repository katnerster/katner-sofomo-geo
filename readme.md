Domyślny użytkownik sofomo, hasło sofomo.

# Uruchomienie
- Przygotować plik .env na podstawie .env.example w folderze sofomo
- W katalogu głownym projektu wykonać polecenia
```sh
$ docker-compose build
$ docker-compose up
```
- Aplikacja będzie dostępna pod localhost:8000

# Endpointy

- /api/geolocation/
    - POST przyjmuje pole ip lub url -> pobiera informacje o ip lub url z IPStack, zapisuje je do bazy oraz zwraca. Jeśli informacje istnieją w bazie, zwraca je
    - GET zwraca listę zapisanych informacji
- /api/geolocation/<id>/
    - DELETE usuwa informacje o danym id
    - GET zwraca informacje o danym id
- /api/token-auth/
    - POST przyjmuje pola username oraz password, zwraca token JWT użytkownika 
- /api/token-refresh/
    - POST odświeża podany w json token JWT użytkownika
- /api/token-verify/
    - POST weryfikuje podany w json token JWT użytkownika 

